Processing counts data
=======================

Introduction
------------

Reads overlapping genes were counted using samtools. Output was reported in:

* data/all.alignments_distribute.tsv.gz - summary of alignments
* heat_drought_2010_TH2.0.5_read_counts.txt.gz - counts data, number of reads per gene, per sample

This Markdown reads counts data and writes 

* heat_drought_counts.tsv.gz - counts per gene for single-mapping reads 

Questions:

* How many reads were mapped per libary?
* What was the total number of single-mapping read that mapped onto a gene?

Analysis
--------


### Read data, reformat

```{r}
source('../src/common.R')
fname='data/heat_drought_2010_TH2.0.5_read_counts.txt.gz'
d=read.csv(fname,header=F)
headerrows=which(d[,1]=='gene')
d=d[-headerrows,]
names(d)=c('gene','sample','count')
d$count=as.numeric(as.character(d$count))
library(reshape)
d=cast(d,gene~sample)
names(d)=as.character(sapply(names(d),
                               function(x)
                               {gsub('.sm.bam','',x)}))

d=d[,c('gene',getSamples())] # puts them in a nice order
row.names(d)=d$gene
```

### Summarize counts

```{r fig.width=4.5,fig.height=6.2}
colors=getColors(getSamples())
totals=round(apply(d[,getSamples()],2,sum)/10**6,1)
xlab="Mapped reads per library (millions)"
names(totals)=getSamples()
par(las=1)
par(mar=c(3,3.3,1,1))
par(mgp=c(1.5,0.3,0))
barplot(as.numeric(totals),col=colors,horiz=T,names.arg=getSamples(),
        xlim=c(0,max(totals)+5),
        cex.names=0.8,main="",xlab=xlab)
box()
text(15,18,"Sequencing Depth",pos=4,cex=1)
fname='results/sequencingDepth.pdf'
quartz(file=fname,type='pdf',width=4.5,height=6.2)
par(las=1)
par(mar=c(3,3.3,1,1))
par(mgp=c(1.5,0.3,0))
barplot(as.numeric(totals),col=colors,horiz=T,names.arg=getSamples(),
        xlim=c(0,max(totals)+5),
        cex.names=0.8,main="",xlab=xlab)
box()
text(12,18,"Sequencing Depth",pos=4,cex=1.7)
dev.off()
totals
```

Some samples had not very many counts. We might want to discard these in later steps.

### Sanity check. 

The gene AT1G07350 (SR45A) should be up in drought and heat, down in the controls, normalized by reads per library:

```{r fig.width=4.5,fig.height=6.2}
test.gene='AT1G07350'
vals=d[test.gene,getSamples()]/apply(d[,getSamples()],2,sum)*10**6
par(las=1)
#par(mar=c(5.1,4.1,4.1,2.1))
par(mar=c(3,3.3,1,1))
par(mgp=c(1.5,0.3,0))
bp=barplot(as.numeric(vals),col=colors,horiz=T,names.arg=getSamples(),xlim=c(0,max(vals)+20),
        main="",xlab='Reads Per Million',cex.axis=0.8,cex.names=0.8)
text(275,27,test.gene,cex=1)
box()
fname='results/AT1G07350_RPM.pdf'
quartz(file=fname,type='pdf',width=4.5,height=6.2)
par(las=1)
#par(mar=c(5.1,4.1,4.1,2.1))
par(mar=c(3,3.3,1,1))
par(mgp=c(1.5,0.3,0))
bp=barplot(as.numeric(vals),col=colors,horiz=T,names.arg=getSamples(),xlim=c(0,max(vals)+20),
        main="",xlab='Reads Per Million',cex.axis=0.8,cex.names=0.8)
text(260,27,test.gene,cex=1.5)
box()
dev.off()
```

Write data:

```{r}
fname='results/heat_drought_counts.tsv'
write.table(d,fname,sep='\t',quote=F,row.names=F)
system(paste('gzip -f',fname))
```

Total reads obtained:

```{r}
Total=sum(totals)
```

Conclusion
----------

* Mapped reads per libary, in millions:

```{r}
totals
```

* There were `r Total` million mapped reads.

Limitations
-----------

Some reads from overlapping genes were counted twice.

Session Information
-------------------

```{r}
sessionInfo()
```

