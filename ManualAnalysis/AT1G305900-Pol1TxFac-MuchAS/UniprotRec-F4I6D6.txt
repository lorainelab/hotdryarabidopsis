ID   F4I6D6_ARATH            Unreviewed;       604 AA.
AC   F4I6D6;
DT   28-JUN-2011, integrated into UniProtKB/TrEMBL.
DT   28-JUN-2011, sequence version 1.
DT   05-JUL-2017, entry version 43.
DE   SubName: Full=RNA polymerase I specific transcription initiation factor RRN3 protein {ECO:0000313|EMBL:AEE31247.1};
GN   OrderedLocusNames=At1g30590 {ECO:0000313|Araport:AT1G30590,
GN   ECO:0000313|EMBL:AEE31247.1};
GN   ORFNames=T5I8.4 {ECO:0000313|EMBL:AEE31247.1},
GN   T5I8_4 {ECO:0000313|EMBL:AEE31247.1};
OS   Arabidopsis thaliana (Mouse-ear cress).
OC   Eukaryota; Viridiplantae; Streptophyta; Embryophyta; Tracheophyta;
OC   Spermatophyta; Magnoliophyta; eudicotyledons; Gunneridae;
OC   Pentapetalae; rosids; malvids; Brassicales; Brassicaceae; Camelineae;
OC   Arabidopsis.
OX   NCBI_TaxID=3702 {ECO:0000313|EMBL:AEE31247.1, ECO:0000313|Proteomes:UP000006548};
RN   [1] {ECO:0000313|EMBL:AEE31247.1, ECO:0000313|Proteomes:UP000006548}
RP   NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA].
RC   STRAIN=cv. Columbia {ECO:0000313|Proteomes:UP000006548};
RX   PubMed=11130712; DOI=10.1038/35048500;
RA   Theologis A., Ecker J.R., Palm C.J., Federspiel N.A., Kaul S.,
RA   White O., Alonso J., Altafi H., Araujo R., Bowman C.L., Brooks S.Y.,
RA   Buehler E., Chan A., Chao Q., Chen H., Cheuk R.F., Chin C.W.,
RA   Chung M.K., Conn L., Conway A.B., Conway A.R., Creasy T.H., Dewar K.,
RA   Dunn P., Etgu P., Feldblyum T.V., Feng J., Fong B., Fujii C.Y.,
RA   Gill J.E., Goldsmith A.D., Haas B., Hansen N.F., Hughes B., Huizar L.,
RA   Hunter J.L., Jenkins J., Johnson-Hopson C., Khan S., Khaykin E.,
RA   Kim C.J., Koo H.L., Kremenetskaia I., Kurtz D.B., Kwan A., Lam B.,
RA   Langin-Hooper S., Lee A., Lee J.M., Lenz C.A., Li J.H., Li Y., Lin X.,
RA   Liu S.X., Liu Z.A., Luros J.S., Maiti R., Marziali A., Militscher J.,
RA   Miranda M., Nguyen M., Nierman W.C., Osborne B.I., Pai G.,
RA   Peterson J., Pham P.K., Rizzo M., Rooney T., Rowley D., Sakano H.,
RA   Salzberg S.L., Schwartz J.R., Shinn P., Southwick A.M., Sun H.,
RA   Tallon L.J., Tambunga G., Toriumi M.J., Town C.D., Utterback T.,
RA   Van Aken S., Vaysberg M., Vysotskaia V.S., Walker M., Wu D., Yu G.,
RA   Fraser C.M., Venter J.C., Davis R.W.;
RT   "Sequence and analysis of chromosome 1 of the plant Arabidopsis
RT   thaliana.";
RL   Nature 408:816-820(2000).
RN   [2] {ECO:0000313|EMBL:AEE31247.1, ECO:0000313|Proteomes:UP000006548}
RP   GENOME REANNOTATION.
RC   STRAIN=cv. Columbia {ECO:0000313|Proteomes:UP000006548};
RG   The Arabidopsis Information Portal (Araport);
RL   Submitted (MAY-2016) to the EMBL/GenBank/DDBJ databases.
CC   -----------------------------------------------------------------------
CC   Copyrighted by the UniProt Consortium, see http://www.uniprot.org/terms
CC   Distributed under the Creative Commons Attribution-NoDerivs License
CC   -----------------------------------------------------------------------
DR   EMBL; CP002684; AEE31247.1; -; Genomic_DNA.
DR   RefSeq; NP_174347.1; NM_102796.2.
DR   UniGene; At.65753; -.
DR   ProteinModelPortal; F4I6D6; -.
DR   STRING; 3702.AT1G30590.1; -.
DR   PaxDb; F4I6D6; -.
DR   PRIDE; F4I6D6; -.
DR   EnsemblPlants; AT1G30590.1; AT1G30590.1; AT1G30590.
DR   GeneID; 839939; -.
DR   Gramene; AT1G30590.1; AT1G30590.1; AT1G30590.
DR   Araport; AT1G30590; -.
DR   TAIR; locus:2204609; AT1G30590.
DR   eggNOG; KOG2434; Eukaryota.
DR   eggNOG; ENOG410XRJJ; LUCA.
DR   InParanoid; F4I6D6; -.
DR   OMA; CELAIRH; -.
DR   OrthoDB; EOG09360JPE; -.
DR   Proteomes; UP000006548; Chromosome 1.
DR   ExpressionAtlas; F4I6D6; baseline and differential.
DR   GO; GO:0005634; C:nucleus; IBA:GO_Central.
DR   GO; GO:0001042; F:RNA polymerase I core binding; IBA:GO_Central.
DR   GO; GO:0001181; F:transcription factor activity, core RNA polymerase I binding; IBA:GO_Central.
DR   GO; GO:0003743; F:translation initiation factor activity; IEA:UniProtKB-KW.
DR   GO; GO:0001180; P:transcription initiation from RNA polymerase I promoter for nuclear large rRNA transcript; IBA:GO_Central.
DR   Gene3D; 1.25.10.10; -; 1.
DR   InterPro; IPR011989; ARM-like.
DR   InterPro; IPR007991; RNA_pol_I_trans_ini_fac_RRN3.
DR   PANTHER; PTHR12790; PTHR12790; 1.
DR   Pfam; PF05327; RRN3; 1.
PE   4: Predicted;
KW   Complete proteome {ECO:0000313|Proteomes:UP000006548};
KW   Initiation factor {ECO:0000313|EMBL:AEE31247.1};
KW   Protein biosynthesis {ECO:0000313|EMBL:AEE31247.1};
KW   Reference proteome {ECO:0000313|Proteomes:UP000006548}.
SQ   SEQUENCE   604 AA;  68065 MW;  7DFA03410EC99C54 CRC64;
     MGAEEFPSVP FNSNAMDNAE YTDTDLVFAV RKALASVQNG DTDDYSQLKT VMCLTEDADF
     DAVAQLETVL KSLSVSVAWI DLVHHKDLLE AMSLWYHSHR PSVMDALVDL IISLAATSGK
     YLDPCLNMLV RNFSQPTFKH KVSQTQLVKK MQEVHPRVHA ALHKISYLIP LAPWNLVSIL
     AQNMRKIDKK DPSIVTYVDN LLRLENSSIG EVVGSVILMM VMERMLDLDL VSGCDDSNGG
     MFDMELEDAV ESTMNEGDEF PVGALKQNTS GGNVVSELLD KLMVLFFHHL ESCQNSDRLD
     EVFEILFKSF ENYILNTYKT KFSQFLMFYA CSLDPENCGV RFASKLLDIY LSSNTCRLTR
     MSAVAYLASY LSRGKFLPAS FVASMLKRLV DECAEYCGTC NDDVKPEAHQ VFYSGCQAIL
     YVLCFRMRSI VEIPRFQSQF RSLESILSHK LNPLLVCLPS VVSEFLKQAK AGGLFIVSES
     FIFDDLHESE LSRAFGGFER LDTFFPFDPC LLKMSSSYIS PNFNFWSMVK TTYGEDGDEE
     LCDEVIVNGD ADSAEEPDDD VELDSEMNTM STTPKHSFMR ETERLLKMPS RIRPSTSPPE
     SFLI
//
