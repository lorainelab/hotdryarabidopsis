#!/bin/bash

# shell script that copies stuff from repo into 
# DropBox folder for all the authors to read, review
D=~/Dropbox/AbioticStressRNASeqPaper
C='../Counts'
DE='../DifferentialExpression'
gunzip -c $C/results/heat_drought_counts.tsv.gz > $D/$S-S1.txt
gunzip -c $C/results/heat_drought_RPKM.tsv.gz > $D/$S-S2.txt
gunzip -c $DE/results/WetDry.txt.gz > $D/$S-S3.txt
gunzip -c $DE/results/CoolHotT1.txt.gz  > $D/$S-S4.txt
gunzip -c $DE/results/CoolHotT2.txt.gz > $D/$S-S5.txt
cp $DE/results/tx.txt $D/$S-S6.txt