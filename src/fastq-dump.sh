#!/bin/bash
#PBS -q copperhead
#PBS -l nodes=1:ppn=1
#PBS -l mem=96gb
#PBS -l walltime=24:00:00
cd $PBS_O_WORKDIR
module load sra-tools

# S - sample name
# F - file name
# both need to be passed in using qsub -v option
#
# To run this run using qsub-doIt.sh, do:
#
#  qsub-doIt.sh .sra fastq-dump.sh >jobs.out 2>jobs.err
#
CMD="fastq-dump $F"
$CMD # makes $S.fastq
CMD="gzip $S.fastq"
$CMD
