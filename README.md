# Mild heat and extreme dessication stress alter gene expression and splicing in Arabidopsis #

This repository investigates the effects of heat stress and extreme dessication on gene expression and splicing in plants, using 3-week-old Arabidopsis plants as a model. 

* * * 

**ATTENTION**

These data are unpublished and have not been reviewed or validated
as yet. Anything you find here may change without warning.

**Please do not use data or code in this repository without first
contacting Ann Loraine at aloraine@uncc.edu.**

* * *

# What's here

Analysis modules and other directories include .Rmd files (R
Markdown), output from knitting Rmd files in HTML (Web pages), folders
containing results (results) and (sometimes) folders containing
externally supplied data used only in one module (data). Results folders
also contain high-quality image files suitable for inclusion in slides or publications. 

* * *

## AltSplice

How do stresses affect splicing? 

* * *

## Counts 

How many RNA-Seq reads mapped onto which genes in which sample? Which and how many genes were expressed? How did heat and dessication stresses affect gene expression? 

Makes files reporting expression data as counts or RPKM files. 

* * *

## CufflinksAnalysis

Can we use cufflinks tools to assess splicing? We could not. In our hands, we observed that cufflinks fused neighboring genes and produced odd results. We decided to drop it.

* * *

## DifferentialExpression

How did dessication and heat stress change gene expression? 

Makes files with differential expression testing results.

Look here for differential expression analysis results.

* * *

## ExternalDataSets

Contains Arabidopsis annotations and other data files downloaded from 
other sites. 

Contains tab- and comma-delimited files for Sequence Read Archive accessions and sample descriptions.

* * *

## DisruptedSplicing

Do extreme or mild stresses disrupt splicing?

* * *

## GOSeq

Uses GOSeq to identifies over and under represented GO, MapMan, AraCyc
categories and pathways among differentially expressed genes identifed
in the DifferentialExpression module.

This module also contains code that makes gene to category data files
for AraCyc.

* * * 

## GeneRegions

Builds SAF.txt file needed for featureCounts, used to generate reads per
gene counts files.

Examines distribution of intron sizes in gene model annotations, required
to determine maximum intron size prarameter for running spliced alignment tools.

* * *

## ManualAnalysis

Notes and genome browser images for genes examined manually.

* * *

## SampleComparison

Compares samples using gene expression profiles.

* SampleComparison.Rmd - clusters samples by RPKM expression

See SampleComparison/results for dendrograms illustrating sample
similarities.

* * * 

## src

Contains python, R, shell scripts and Java archive (jar) files with
code used for data processing.

# Questions? 

Contact:

* Ann Loraine aloraine@uncc.edu
* Nowlan Freese nfreese@uncc.edu

* * *

# License

All rights reserved pending formal publication. 