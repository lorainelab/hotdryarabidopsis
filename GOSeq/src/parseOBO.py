#!/usr/bin/env python

why_and_what="""
To do GO term enrichment analysis using GOSeq, we need the GO term definitions (gene_ontology_ext.obo) reformatted into tabular form. This program does that conversion. It can read the file given as an argument on the command line or piped from stdin. It then writes tab-delimited output to stdout.  Adapted from a program by Vikas Gupta.
"""
how="""
ex) gunzip -c gene_ontology_ext.obo.gz | %(prog)s > go_defs.tsv"""

import os,sys,argparse,re,fileinput

def main(file=None,
         verbose=False):

    # This part adapted from VG
    first_line = True
    
    ### column headings
    heads=['GO_id','name','namespace','def']
    cols=heads[1:] # fields in the OBO file
    
    ### write to stdout
    out = sys.stdout
    
    ### print header
    out.write('\t'.join(heads)+'\n')
    
    ### open the file
    for line in fileinput.input(file):
        line = line.strip()
        if line.startswith("[Typedef]"):
            # at the end of the file
            break
        if line.startswith("[Term]"):                  ### check for new section 
            if first_line == False:
                out.write('\n')
            first_line = False
        elif line.startswith('id:'):
            token = line.split('id:')
            out.write(token[1].strip())
        elif line.startswith('def:'):
            lst=which(line)
            if verbose:
                if len(lst)>2:
                    sys.stderr.write("Warning: Extra quotes:%s\n"%line)
            defn=line[lst[0]+1:lst[-1]]
            out.write('\t'+defn)
        else: # the other fields we care about
            token = line.split(':')
            if token[0] in cols:
                token = line.split(token[0]+':')
                out.write('\t'+token[1].strip())
    out.write('\n')

# get a list of places where a character occurs in a string
def which(s,c='"'):
    lst=[]
    index=0
    while index<len(s):
        index = s.find(c,index)
        if index == -1: # didn't find it
            break
        else:
            lst.append(index)
            index=index+1
    return lst

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=why_and_what,
                                     epilog=how)
    parser.add_argument('-v','--verbose',
                        default=False,
                        action='store_true',
                        dest="verbose",
                        help='print warnings to stderr [optional]')

    parser.add_argument('file',nargs='?',default='-',
                        help='name GO obo file [optional]')
    args = parser.parse_args()
    main(file=args.file,
         verbose=args.verbose)
