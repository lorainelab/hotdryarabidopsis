```{r echo=FALSE}
tested='drought'
```

Category enrichment analysis of `r tested`-regulated genes
========================================================

* * * 

Introduction
------------

Many genes were differentially expressed by drought treatment. Here, we categories of genes that contained unusually many or unusually few DE genes.

Questions:

* Which MapMan and GO categories contained unusually many or unusually few DE genes?
* Which AraCyc pathways contained unusually many or few DE genes?

* * *

Analysis
--------

### Setting up

Load code and DE genes:

```{r}
source('../src/common.R')
de_results=getDroughtDiffExprResults()
de_FDR=getDroughtDeFDR()
```

* * * 

```{r}
system='GO'
```

### `r system` analysis

Load gene to `r system` category mapping:

```{r}
gene2cat=getGeneToGOMapping()
```

Test for enriched or depleted categories:

```{r fig.width=3,fig.height=3}
cat_FDR=de_FDR
res=testCategories(de_FDR=de_FDR,cat_FDR=cat_FDR,gene2cat=gene2cat,
                   de_results=de_results)
```

How many genes with `r system` annotations were DE?

```{r}
denom=length(intersect(unique(gene2cat$gene),de_results$gene))
numer=length(intersect(unique(gene2cat$gene),de_results[de_results$fdr<=de_FDR,'gene']))
total_percent=round(numer/denom*100,1)
```

Recall that we did not test every gene for differential expression - only the genes where we observed at least one read. We observed at least one read for `r nrow(de_results)` genes. Of these, there were `r denom` genes with at least one `r system` annotation. Of the genes that we tested for differential expression and had a GO annotation, there were `r numer` that were differentially expressed with FDR `r de_FDR` or less. Thus, `r total_percent`% of `r system`-annotated, expressed genes were also differentially expressed.

If all DE genes are distributed evenly to all `r system` categories and a gene can be annotated to multiple categories, then we expect that on average, categories will contain around `r total_percent`% DE genes. 

Examine distribution of percentages among the enriched and depleted categories:

```{r fig.height=3,fig.width=3}
h=hist(res$percent_de,plot=F)
top=max(h$counts)+5
plot(h,main=paste(system,"annotations"),xlab="percent DE",
     label=T,col="lightblue",ylim=c(0,top))
abline(v=total_percent,col="magenta")
fname=paste0('results/',system,'-hist-',tested,'.png')
quartz.save(fname,dpi=300,width=3,height=3)
```

Categories with an unusually large percentage of DE genes: `r res[res$over_fdr<=cat_FDR,]$term`.

Categories with an unusually small percentage of DE genes: `r res[res$under_fdr<=cat_FDR,]$term`.

#### Examine DE genes in over-represented categories

Are genes in over-represented categories mostly up or down regulated?

```{r}
de=de_results[de_results$fdr<=de_FDR,]$gene
CatPlusGenes=merge(res[res$over_fdr<=cat_FDR,],
                  gene2cat[gene2cat$gene%in%de,],
                  by.x='category',by.y='category')
CatPlusGenes=merge(CatPlusGenes,
                  de_results[,c('gene','logFC','descr')],
                  by.x='gene',by.y='gene')
o=order(CatPlusGenes$percent_de,CatPlusGenes$term,decreasing=T)
v=rep(0,nrow(CatPlusGenes))
v[CatPlusGenes$logFC>0]=1
v[CatPlusGenes$logFC<0]=-1
CatPlusGenes$direction=v
CatPlusGenes=CatPlusGenes[o,c('category','percent_de','direction',
                            'term','gene','descr')]
fname=paste0('results/',system,'-plusGenes-',tested,'.txt')
write.table(CatPlusGenes,file=fname,
            row.names=F,quote=F,sep='\t')
# it's big so compress it
system(paste('gzip -f',fname))
```

What percentage of DE genes were up-regulated?

```{r}
res$percent_up=rep(NA,nrow(res))
v=which(res$over_fdr<=cat_FDR)
for (i in v) {
  category=res[i,'category']
  up=sum(CatPlusGenes[CatPlusGenes$category==category,]$direction==1)
  res[i,'percent_up']=round(up/res[i,'numDEInCat']*100,1)
}
res=res[,c('category','numDEInCat',
         'numInCat','percent_de',
         'percent_up',
         'over_fdr','under_fdr','term')]
```

Write a file:

```{r}
fname=paste0('results/',system,'-',tested,'.txt')
res=res[order(res$percent_de,decreasing=T),]
write.table(res,file=fname,row.names=F,quote=T,sep="\t")
```

#### Summary

This analysis created a tab-separated file named `r fname` and identified `r length(which(res$over_fdr<=cat_FDR))` enriched `r system` categories and `r length(which(res$under_fdr<=cat_FDR))` under-represented `r system` categories using category FDR `r cat_FDR`.

Many terms and pathways were enriched with differentially expressed genes. Most notable were:

* pathways and functions related to photosynthesis, which were down-regulated


* * * 

```{r}
system='AraCyc'
```

### `r system` analysis

Test `r system` category enrichment:

```{r fig.width=3,fig.height=3}
gene2cat=getGeneToAraCycMapping()
cat_FDR=0.1
res=testCategories(de_FDR=de_FDR,cat_FDR=cat_FDR,gene2cat=gene2cat,
                   de_results=de_results)
cat2term=gene2cat[,c('category','term')]
cat2term=cat2term[!duplicated(paste(cat2term$category,cat2term$term)),]
res=merge(res,cat2term,by.x='category',by.y='category')
```

How many genes with `r system` annotations are DE?

```{r}
denom=length(intersect(unique(gene2cat$gene),de_results$gene))
numer=length(intersect(unique(gene2cat$gene),de_results[de_results$fdr<=de_FDR,'gene']))
total_percent=round(numer/denom*100,1)
```

Recall that we did not test every gene for differential expression - only the genes where we observed at least one read. We observed at least one read for `r nrow(de_results)` genes. Of these, there were `r denom` genes with at least one `r system` annotation. Of the genes that we tested for differential expression and had a GO annotation, there were `r numer` that were differentially expressed with FDR `r de_FDR` or less. Thus, `r total_percent`% of `r system`-annotated, expressed genes were also differentially expressed.

If all DE genes are distributed evenly to all `r system` categories, keeping in mind that a gene can be annotated to multiple categories, then we expect that on average, categories will contain around `r total_percent`% DE genes. 

Examine distribution of percentages among the enriched and depleted categories:

```{r fig.height=3,fig.width=3}
h=hist(res$percent_de,plot=F)
top=max(h$counts)+5
plot(h,main=paste(system,"annotations"),xlab="percent DE",
     label=T,col="lightblue",ylim=c(0,top))
abline(v=total_percent,col="magenta")
fname=paste0('results/',system,'-hist-',tested,'.png')
quartz.save(fname,dpi=300,width=3,height=3)
cats_large=res[res$over_fdr<=cat_FDR,]
cats_large=paste(cats_large$category,
                 cats_large$term)
cats_small=res[res$under_fdr<=cat_FDR,]
cats_small=paste(cats_small$category,
                 cats_small$term)
```

Categories with an unusually large percentage of DE genes: `r cats_large`.

Categories with an unusually small percentage of DE genes: `r cats_small`.

#### Examine DE genes in over-represented categories

Are genes in over-represented categories mostly up or down regulated?

```{r}
de=de_results[de_results$fdr<=de_FDR,]$gene
CatPlusGenes=merge(res[res$over_fdr<=cat_FDR,],
                  gene2cat[gene2cat$gene%in%de,c('category','gene')],
                  by.x='category',by.y='category')
CatPlusGenes=merge(CatPlusGenes,
                  de_results[,c('gene','logFC','descr')],
                  by.x='gene',by.y='gene')
o=order(CatPlusGenes$percent_de,CatPlusGenes$term,decreasing=T)
v=rep(0,nrow(CatPlusGenes))
v[CatPlusGenes$logFC>0]=1
v[CatPlusGenes$logFC<0]=-1
CatPlusGenes$direction=v
CatPlusGenes=CatPlusGenes[o,c('category','percent_de','direction',
                            'term','gene','descr')]
fname=paste0('results/',system,'-plusGenes-',tested,'.txt')
write.table(CatPlusGenes,file=fname,
            row.names=F,quote=F,sep='\t')
```

What is percentage of DE genes that were up-regulated?

```{r}
res$percent_up=rep(NA,nrow(res))
v=which(res$over_fdr<=cat_FDR)
for (i in v) {
  category=res[i,'category']
  up=sum(CatPlusGenes[CatPlusGenes$category==category,]$direction==1)
  res[i,'percent_up']=round(up/res[i,'numDEInCat']*100,1)
}
res=res[,c('category','numDEInCat',
         'numInCat','percent_de',
         'percent_up',
         'over_fdr','under_fdr','term')]
```

Write a file:

```{r}
fname=paste0('results/',system,'-',tested,'.txt')
res=res[order(res$percent_de,decreasing=T),]
write.table(res,file=fname,row.names=F,quote=T,sep="\t")
```

#### Summary

This analysis created a tab-separated file named `r fname` and identified `r length(which(res$over_fdr<=cat_FDR))` enriched `r system` categories and `r length(which(res$under_fdr<=cat_FDR))` under-represented `r system` categories using category FDR `r cat_FDR`.

Pathways related to photosynthesis were enriched with differentially expressed genes.  

* * * 

```{r}
system='MapMan'
```

### `r system` analysis

Test `r system` category enrichment:

```{r fig.width=3,fig.height=3}
gene2cat=getGeneToMapManMapping()
cat_FDR=0.01
res=testCategories(de_FDR=de_FDR,cat_FDR=cat_FDR,gene2cat=gene2cat,
                   de_results=de_results)
```

How many genes with `r system` annotations are DE?

```{r}
denom=length(intersect(unique(gene2cat$gene),de_results$gene))
numer=length(intersect(unique(gene2cat$gene),de_results[de_results$fdr<=de_FDR,'gene']))
total_percent=round(numer/denom*100,1)
```

Recall that we did not test every gene for differential expression - only the genes where we observed at least one read. We observed at least one read for `r nrow(de_results)` genes. Of these, there were `r denom` genes with at least one `r system` annotation. Of the genes that we tested for differential expression and had a GO annotation, there were `r numer` that were differentially expressed with FDR `r de_FDR` or less. Thus, `r total_percent`% of `r system`-annotated, expressed genes were also differentially expressed.

If all DE genes are distributed evenly to all `r system` categories, keeping in mind that a gene can be annotated to multiple categories, then we expect that on average, categories will contain around `r total_percent`% DE genes. 

Examine distribution of percentages among the enriched and depleted categories:

```{r fig.height=3,fig.width=3}
h=hist(res$percent_de,plot=F)
top=max(h$counts)+5
plot(h,main=paste(system,"annotations"),xlab="percent DE",
     label=T,col="lightblue",ylim=c(0,top))
abline(v=total_percent,col="magenta")
fname=paste0('results/',system,'-hist-',tested,'.png')
quartz.save(fname,dpi=300,width=3,height=3)
```

Categories with an unusually large percentage of DE genes: `r res[res$over_fdr<=cat_FDR,]$category`.

Categories with an unusually small percentage of DE genes: `r res[res$under_fdr<=cat_FDR,]$category`.

#### Examine DE genes in over-represented categories

Are genes in over-represented categories mostly up or down regulated?

```{r}
de=de_results[de_results$fdr<=de_FDR,]$gene
CatPlusGenes=merge(res[res$over_fdr<=cat_FDR,],
                  gene2cat[gene2cat$gene%in%de,],
                  by.x='category',by.y='category')
CatPlusGenes=merge(CatPlusGenes,
                  de_results[,c('gene','logFC','descr')],
                  by.x='gene',by.y='gene')
o=order(CatPlusGenes$percent_de,CatPlusGenes$category,decreasing=T)
v=rep(0,nrow(CatPlusGenes))
v[CatPlusGenes$logFC>0]=1
v[CatPlusGenes$logFC<0]=-1
CatPlusGenes$direction=v
CatPlusGenes=CatPlusGenes[o,c('category','percent_de','direction',
                            'gene','descr')]
fname=paste0('results/',system,'-plusGenes-',tested,'.txt')
write.table(CatPlusGenes,file=fname,
            row.names=F,quote=F,sep='\t')
```

What is percentage of DE genes that were up-regulated?

```{r}
res$percent_up=rep(NA,nrow(res))
v=which(res$over_fdr<=cat_FDR)
for (i in v) {
  category=res[i,'category']
  up=sum(CatPlusGenes[CatPlusGenes$category==category,]$direction==1)
  res[i,'percent_up']=round(up/res[i,'numDEInCat']*100,1)
}
res=res[,c('category','numDEInCat',
         'numInCat','percent_de',
         'percent_up',
         'over_fdr','under_fdr')]
```

Write a file:

```{r}
fname=paste0('results/',system,'-',tested,'.txt')
res=res[order(res$percent_de,decreasing=T),]
write.table(res,file=fname,row.names=F,quote=T,sep="\t")
```

#### Summary

This analysis created a tab-separated file named `r fname` and identified `r length(which(res$over_fdr<=cat_FDR))` enriched `r system` categories and `r length(which(res$under_fdr<=cat_FDR))` under-represented `r system` categories using category FDR `r cat_FDR`.

Several MapMan categories contained unusually many or few DE genes. Genes related to photosynthesis were generally down-regulated, many LRR kinase genes were down-regulated, and genes involved in hormone signaling were disrupted.

Conclusions
-----------

Genes related to photosynthesis were generally down-regulated. Hormone pathways were disrupted. Signaling pathways using LRR kinases were disrupted. 


Session Information
-------------------

```{r}
sessionInfo()
```
